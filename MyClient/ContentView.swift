//
//  ContentView.swift
//  MyClient
//
//  Created by C Beers on 7/30/22.
//

import SwiftUI

struct ContentView: View {
    @State var serverGreeting = ""
    var body: some View {
        Text(serverGreeting)
            .padding()
            .task {
                do {
                    let mySeverClient = MyServerClient()
                    self.serverGreeting = try await mySeverClient.greet()
                } catch {
                    self.serverGreeting = String(describing: error)
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

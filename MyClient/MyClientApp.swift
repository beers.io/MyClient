//
//  MyClientApp.swift
//  MyClient
//
//  Created by C Beers on 7/30/22.
//

import SwiftUI

@main
struct MyClientApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

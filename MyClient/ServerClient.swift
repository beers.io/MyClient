//
//  ServerClient.swift
//  MyClient
//
//  Created by C Beers on 7/30/22.
//

import Foundation

struct MyServerClient {
    let baseURL = URL(string: "http://127.0.0.1:8080")!
    
    func greet() async throws -> String {
        let url = baseURL.appendingPathComponent("greet")
        let (data, _) = try await URLSession.shared.data(for: URLRequest(url: url))
        guard let responseBody = String(data: data, encoding: .utf8) else {
            throw Errors.invalidResponseEncoding
        }
        return responseBody
    }
    
    enum Errors: Error {
        case invalidResponseEncoding
    }
}
